from django.apps import AppConfig


class WifscraperConfig(AppConfig):
    name = 'wifscraper'
