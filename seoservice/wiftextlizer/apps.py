from django.apps import AppConfig


class WiftextlizerConfig(AppConfig):
    name = 'wiftextlizer'
