certifi==2017.7.27.1
chardet==3.0.4
DAWG-Python==0.7.2
Django==1.11
docopt==0.6.2
idna==2.6
jsonfield==2.0.2
jusText==2.2.0
lxml==4.0.0
numpy==1.13.1
pymorphy2==0.8
pymorphy2-dicts==2.4.393442.3710985
pymorphy2-dicts-ru==2.4.404381.4453942
pymorphy2-dicts-uk==2.4.1.1.1460299261
pytz==2017.2
requests==2.18.4
scikit-learn==0.19.0
scipy==0.19.1
selenium==3.5.0
sklearn==0.0
urllib3==1.22
