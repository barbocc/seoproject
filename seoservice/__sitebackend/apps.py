from django.apps import AppConfig


class SitebackendConfig(AppConfig):
    name = 'sitebackend'
