from django.apps import AppConfig


class WifsubjectsConfig(AppConfig):
    name = 'wifsubjects'
