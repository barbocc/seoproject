from json import dumps
from django.contrib import admin
from .models import Database

# Register your models here.
def config(obj):
    return dumps(obj.config)
config.short_description = 'Конфиг'

def name(obj):
    return dumps(obj.name)

name.short_description = 'Название'

class DatabaseAdmin(admin.ModelAdmin):
    list_display = (name, config)


admin.site.register(Database, DatabaseAdmin)
