from __future__ import unicode_literals

from logging import getLogger
from os.path import join
from tempfile import gettempdir
from io import StringIO


from django.core.management.commands.inspectdb import Command
from django.conf import settings
from django.db import models, connections
from django.apps import AppConfig as DjAppConfig
from django.apps.registry import apps
from django import VERSION

from jsonfield import JSONField

logger = getLogger('dynamic_databases.models')


class AppConfig(DjAppConfig):
    def __init__(self, label):
        self.path = join(gettempdir(), label)
        super(AppConfig, self).__init__(label, label)
        self.apps = apps

# Create your models here.
class Database(models.Model):
    name = models.CharField(max_length=256, unique=True)
    config = JSONField()

    def __unicode__(self):
        return self.name

    def get_model(self, table_name):
        # Ensure the database connect and it's dummy app are registered
        self.register()
        label = self.label
        model_name = table_name.lower().replace('_', '')
        print('==========================')
        #print(vars(apps.all_models[label]))
        # Is the model already registered with the dummy app?
        if model_name not in apps.all_models[label]:
            logger.info('Adding dynamic model: %s %s', label, table_name)
            # Use the "inspectdb" management command to get the structure of the table for us.
            file_obj = StringIO()
#            #StringIO()
            kwargs = {
                'database': self.label,
                'table_name_filter': lambda t: t == table_name
            }
            if VERSION[0] >= 1 and VERSION[1] >= 10:
                kwargs['table'] = [table_name]
#            Command(stdout=file_obj).handle(
#                database=label, table_name_filter=lambda t: t == table_name
#            )
            apps.apps_ready = apps.ready = True
            Command(stdout=file_obj).handle(**kwargs)
#            model_definition = file_obj.data
            model_definition = file_obj.getvalue()
            file_obj.close()

            # Make sure that we found the table and have a model definition
            loc = model_definition.find('(models.Model):')
            if loc != -1:
                # Ensure that the Model has a primary key.
                # Django doesn't support multiple column primary keys,
                # So we have to add a primary key if the inspect command didn't
                if model_definition.find('primary_key', loc) == -1:
                    loc = model_definition.find('(', loc + 14)
                    model_definition = '{}primary_key=True, {}'.format(
                        model_definition[:loc + 1], model_definition[loc + 1:]
                    )
                # Ensure that the model specifies what app_label it belongs to
                loc = model_definition.find('db_table = \'{}\''.format(table_name))
                if loc != -1:
                    model_definition = '{}app_label = \'{}\'\n        {}'.format(
                        model_definition[:loc], label, model_definition[loc:]
                    )
                # Register the model with Django. Sad day when we use 'exec'
#                print(vars(self))
                apps.models_ready = True
                #print(model_definition)
                exec(model_definition, globals(), locals())
                # Update the list of models that the app
                # has to match what Django now has for this app
                apps.app_configs[label].models = apps.all_models[label]
            else:
                logger.info('Could not find table: %s %s', label, table_name)
        else:
            logger.info('Already added dynamic model: %s %s', label, table_name)
        # If we have the connection, app and model. Return the model class
        if (
                            label in connections._databases and
                            label in apps.all_models and
                        model_name in apps.all_models[label]
        ):
            return apps.get_model(label, model_name)

    def register(self):
        # label for the database connection and dummy app
        label = self.label
        # Do we have this database registered yet
        if label not in connections._databases:
            # Register the database
            connections._databases[label] = self.config
            # Break the cached version of the database dict so it'll find our new database
            del connections.databases
        # Have we registered our fake app that'll hold the models for this database
        if label not in apps.app_configs:
            # We create our own AppConfig class, because the Django one needs a path to the module that is the app.
            #Our dummy app obviously doesn't have a path
            app_config = AppConfig(self.label)
#            app_config = AppConfig2(label, label)
#            app_config = AppConfig(self.label)
            # Manually register the app with the running Django instance
            apps.app_configs[label] = app_config
            apps.app_configs[label].models = {}

    def unregister(self):
        label = self.label
        logger.info('Unregistering Database, app and all related models: "%s"', label)
        if label in apps.app_configs:
            del apps.app_configs[label]
        if label in apps.all_models:
            del apps.all_models[label]
        if label in connections._databases:
            del connections._databases[label]
            del connections.databases

    @property
    def label(self):
        # We want to be able to identify the dynamic databases and apps
        # So we prepend their names with a common string
        prefix = getattr(settings, 'DYNAMIC_DATABASES_PREFIX', 'DYNAMIC_DATABASE')
        separator = getattr(settings, 'DYNAMIC_DATABASES_SEPARATOR', '_')
        return '{}{}{}'.format(prefix, separator, self.pk)