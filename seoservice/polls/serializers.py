from rest_framework import serializers
from polls.contentanalizer import


class ArticleSerializer(serializers.Serializer):

    # Текст статьи
    text = serializers.CharField(required=True, allow_blank=True, max_length=20000)

    # Источники
    url_0 = serializers.CharField(required=True, allow_blank=True, max_length=200)
    url_1 = serializers.CharField(required=True, allow_blank=True, max_length=200)
    url_2 = serializers.CharField(required=True, allow_blank=True, max_length=200)
    url_3 = serializers.CharField(required=True, allow_blank=True, max_length=200)
    url_4 = serializers.CharField(required=True, allow_blank=True, max_length=200)

    def create(self, validated_data):
        """
        Создаёт новый экзэмпляр
        :param validated_data:
        :return:
        """

    return

