from django.views import generic
from dynamic_databases.models import Database
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.paginator import Paginator
import re
import pymorphy2
from sklearn.feature_extraction.text import CountVectorizer
from polls.forms import SearchForm
from polls.contentscrapping import ContentScrapping
from polls.contentanalizer import ContentAnalizer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from polls.serializers import ArticleSerializer
from django.http import JsonResponse


class HomeView(generic.TemplateView):
    template_name = 'polls/home.html'

    def get_context_data(self):
        context = super(HomeView, self).get_context_data()

        # We can pick which dynamic database connection we want based on a GET parameter
        #print(vars(self.request.GET.get('env', 1)))
        #db = Database.objects.get(pk=self.request.GET.get('env', 1))

        db = Database.objects.get(id='1')
        #db = Database.objects.order_by('id')[:1]
        # Pass the database instance to the template so we can display it.
        context['db'] = db

        # Get a model class for a table in our dynamic database.
        # Lets pretend there's a table called 'author'
        WPPost = db.get_model('k2ccuqE_posts')
        kwargs = {
            'post_type': 'post',
            'post_status': 'publish'
        }
        wpposts = WPPost.objects.filter(**kwargs).order_by('id')[:40]
        # Send the author instances to the template for iterating over.
        context['wpposts'] = wpposts
        return context


class PostListView(generic.ListView):
    # paginate_by = 20
    template_name = 'polls/post_list_view.html'
    object_list = []
    # context_object_name = 'wpposts'

    # model
    def get_queryset(self):
        db = Database.objects.get(id='1')
        post_id = 1201
        WPPost = db.get_model('k2ccuqE_posts')
        self.model = WPPost

        kwargs = {
            'post_type': 'post',
            'post_status': 'publish'
        }
        wpposts = WPPost.objects.filter(**kwargs).order_by('id').all()
        paginator = Paginator(wpposts, 10)  # Show 25 contacts per page
        self.paginator = paginator
        page = self.request.GET.get('page')
        try:
            wpposts = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            wpposts = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            wpposts = paginator.page(paginator.num_pages)
        swl=[]
        # Normalize texts
        for wppost in wpposts:
            words_list = []
            morph = pymorphy2.MorphAnalyzer()
            # убираем bbcodes
            wppost.post_content = re.sub('\[caption.*?]', '', wppost.post_content)
            # убираем html
            wppost.post_content = re.sub('<.*?>', '', wppost.post_content)
            # убираем числа
            wppost.post_content = ''.join([i for i in wppost.post_content if not i.isdigit()])
            # разбиваем текст на слова и нормализуем каждое слово
            for word in wppost.post_content.split():
                value = morph.parse(word)[0]
                words_list.append(value.normal_form)
            wppost.post_normal_content = ' '.join(words_list)

            # векторизируем текст
            vectorizer = CountVectorizer(
                ngram_range=(2, 3),
                stop_words=swl,
            )

            analyzer = vectorizer.build_analyzer()
            wppost.post_grams = analyzer(wppost.post_normal_content)
            X = vectorizer.fit_transform([wppost.post_normal_content])
            terms = vectorizer.get_feature_names()
            freqs = X.sum(axis=0).A1
            #print(X.toarray())
            #result = dict(zip(terms, freqs))
            freqs = filter(lambda x: x > 1, freqs)
            result = list(zip(terms, freqs))
            wppost.ngrams = result
            print(result[0])
            #wppost.ngrams.keys = result.keys()
            #wppost.ngrams.values = result.values()
            #print(result.values())
            #print(result.keys())
            #matrix_terms = np.array(vectorizer.get_feature_names())
            #print(wppost.post_grams)
        self.queryset = wpposts
        self.object_list = wpposts
        return self.object_list

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(PostListView, self).get_context_data()
        # Get the blog from id and add it to the context
        context['is_paginated'] = True
        context['paginator'] = self.paginator
        return context


class PostView(generic.TemplateView):

    template_name = 'polls/post.html'

    def get_context_data(self):
        context = super(PostView, self).get_context_data()
        # We can pick which dynamic database connection we want based on a GET parameter
        db = Database.objects.get( id='1')
        #post_id = 1246
        post_id = 1201
        #db = Database.objects.order_by('id')[:1]
        # Pass the database instance to the template so we can display it.
        context['db'] = db

        # Get a model class for a table in our dynamic database.
        # Lets pretend there's a table called 'author'
        WPPost = db.get_model_dyn('k2ccuqE_posts')
        kwargs = {
            'id': post_id,
        }
        morph = pymorphy2.MorphAnalyzer()
        wppost = WPPost.objects.filter(**kwargs).order_by('id')[:1]

        words_list = []
        for word in wppost.post_content.split():
            value = morph.parse(word)[0]
            words_list.append(value.normal_form)

        wppost.post_content = ' '.join(words_list)
        # Send the author instances to the template for iterating over.
        context['wpposts'] = wppost

        return context


class AjaxView:
    """
    Mixin to add AJAX support to a form.
    Must be used with an object-based FormView (e.g. CreateView)
    """
    template_name = 'polls/search.html'

    def form_invalid(self, form):
        response = super(AjaxView, self).form_invalid(form)
        if self.request.is_ajax():
            return JsonResponse(form.errors, status=400)
        else:
            return response

    def form_valid(self, form):
        # We make sure to call the parent's form_valid() method because
        # it might do some processing (in the case of CreateView, it will
        # call form.save() for example).
        response = super(AjaxView, self).form_valid(form)
        print(self.request.is_ajax())
        if self.request.is_ajax():
            data = {
                'pk': 'ololo',
            }
        return JsonResponse(data)


class SearchView(AjaxView, generic.FormView):

    template_name = 'polls/search.html'
    form_class = SearchForm
    success_url = '.'
    test_valid = False

    def get(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        context = self.get_context_data(**kwargs)
        context['form'] = form
        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        print('post')
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        kwargs = {"search_query": request.POST['search_query'],
                  "extra_urls": request.POST['extra_urls'],
                  "stop_domains": request.POST['stop_domains'],
                  "ciphs_law": request.POST['ciphs_law'],
                  "classic_toshnota": request.POST['classic_toshnota'],
                  "academ_toshnota": request.POST['academ_toshnota'],
                  "vodnost": request.POST['vodnost'],
                  "gunning": request.POST['gunning'],
                  "full_of_spam": request.POST['full_of_spam']
                  }
        if form.is_valid():
            return self.form_valid(form, **kwargs)
        #else:
        #    return self.form_invalid(form, **kwargs)

    def form_valid(self, form, **kwargs):
        print('valid')
        if self.request.is_ajax():
            print(kwargs)
            #dataset = ContentAnalizer(kwargs['extra_urls'])
            if kwargs['extra_urls'] != '' and kwargs['search_query'] == '':
                print('extra_urls')
                data = self.form_url_analiz(**kwargs)
                return JsonResponse(data)
            elif kwargs['extra_urls'] == '' and kwargs['search_query'] != '':
                print('search query')
                data = self.form_search(**kwargs)
                return JsonResponse(data)
            #return self.render_to_response(context)

    def form_search(self, **kwargs):
        print('search')
        crapper = None
        crapper = ContentScrapping(kwargs['search_query'], 1)
        #google_urls = crapper.google_urls
        print(crapper.google_urls)
        #yandex_urls = crapper.yandex_urls
        print(crapper.yandex_urls)
        google_data_list = []
        yandex_data_list = []
        print('google_url_len='+str(len(crapper.google_urls)))
        for url in crapper.google_urls:
            print(url)
            dataset = ContentAnalizer(url)
            kwargs['extra_urls'] = url
            if len(url) > 10 and url !=  None:
                google_data_list.append(self.form_url_analiz(**kwargs))

        for url in crapper.yandex_urls:
            print(url)
            dataset = ContentAnalizer(url)
            kwargs['extra_urls'] = url
            if len(url) > 10 and url !=  None:
                yandex_data_list.append(self.form_url_analiz(**kwargs))

        data = {
            'yandex': yandex_data_list,
            'google': google_data_list
        }
        return data

    def form_url_analiz(self, **kwargs):
        print('analiz')
        dataset = ContentAnalizer(kwargs['extra_urls'])
        data = {}
        if dataset.url and kwargs['extra_urls'] != '':
            data['url'] = dataset.url

        if dataset.classic_toshnota and kwargs['classic_toshnota'] == 'on':
            data['classic'] = dataset.classic_toshnota

        if dataset.academ_toshnota and kwargs['academ_toshnota'] == 'on':
            data['academ'] = dataset.academ_toshnota

        if dataset.vodnost and kwargs['vodnost'] == 'on':
            data['vodnost'] = dataset.vodnost

        if dataset.spam and kwargs['full_of_spam'] == 'on':
            data['spam'] = dataset.spam

        if dataset.ciphs_law and kwargs['ciphs_law'] == 'on':
            data['ciphs'] = dataset.ciphs_law

        if dataset.gunning and kwargs['gunning'] == 'on':
            data['gunning'] = dataset.gunning

        if dataset.normal_text_list:
            data['words_count'] = len(dataset.normal_text_list)
        print(data)
        return data

    def form_invalid(self, form, **kwargs):
        print('invalid')


class ArticleCharacteristic(APIView):
    def get_object(self, urls):
        for url in urls:
            try:
                return Snippet.objects.get(url)
            except Snippet.DoesNotExist:
                raise Http404

    def get(self, request, text, url0, url1, url2, url3, url4, format=None):
        serializer = ArticleSerializer(text, url0, url1, url2, url3, url4)
        return Response(serializer.data)
