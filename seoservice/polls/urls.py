from django.conf.urls import url
from . import views
app_name = 'polls'

urlpatterns = [
    #url(r'^posts(?P<page>[0-9]+)/$', views.PostListView.as_view(), name='post_list_view'),
    url(r'^posts/page/(?P<page>[0-9]+)/$', views.PostListView.as_view(), name='post_list_view'),
    url(r'^posts/$', views.PostListView.as_view(), name='post_list_view'),
    url(r'^search/$', views.SearchView.as_view(), name='search'),
    url(r'^expert/$', views.ArticleCharacteristic.as_view(), name='experts'),
    url(r'^$', views.HomeView.as_view(), name='home')
]