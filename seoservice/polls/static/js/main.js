$(function() {
     var a = document.createElement('a');

    // This function gets cookie with a given name
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
    var csrftoken = getCookie('csrftoken');

    /*
    The functions below will create a header with csrftoken
    */

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }
    function sameOrigin(url) {
        // test that a given url is a same-origin URL
        // url could be relative or scheme relative or absolute
        var host = document.location.host; // host + port
        var protocol = document.location.protocol;
        var sr_origin = '//' + host;
        var origin = protocol + sr_origin;
        // Allow absolute or scheme relative URLs to same origin
        return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
            (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
            // or any other URL that isn't scheme relative or absolute i.e relative.
            !(/^(\/\/|http:|https:).*/.test(url));
    }

    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
                // Send the token to same-origin, relative URLs only.
                // Send the token only if the method warrants CSRF protection
                // Using the CSRFToken value acquired earlier
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });

    $('#test').on('click', function(){
        $('#table_of_links').html('');
        $('#table_of_google').html('');
        $('#table_of_yandex').html('');
        // You gotta include the csrf_token in your post data
        event.preventDefault();
        var config = {};
        $('#myform').serializeArray().map(function(item){
            if ( config[item.name] ){
                if ( typeof(config[item.name]) === "string" ){
                    config[item.name] = [config[item.name]];
                }
                config[item.name].push(item.value);
            }
            else {
                if ( item.name === 'extra_urls' ){
                    config[item.name] = item.value.split('\n');
                }
                else{
                    config[item.name] = item.value;
                }
            }
        });

        var csvRows = [
            [
            'URL',
            'Количество слов',
            'Классическая тошнота',
            'Академ. тошнота',
            'Академ. тошнота',
            'Водность',
            'Заспамленность',
            'Закон Ципфа',
            'Ганнинг'
            ]
        ];

         var query = 'csrfmiddlewaretoken='
            + config['csrfmiddlewaretoken'] +

            '&ciphs_law='
            + config['ciphs_law'] +

            '&classic_toshnota='
            + config['classic_toshnota'] +

            '&academ_toshnota='
            + config['academ_toshnota'] +

            '&vodnost='
            + config['vodnost'] +

            '&full_of_spam='
            + config['full_of_spam'] +

            '&gunning='
            + config['gunning'] +

            '&stop_domains='
            + config['stop_domains'];

        console.log( config );
        if ( config['extra_urls'][0] !== ''){
            console.log('for extra urls');
            config['extra_urls'].map(function( item ){
                 console.log(query + '&search_query=&extra_urls='+item);
                 item = item.replace(/^\s+/g, "");
                 item = item.replace("\n", "")
                 item = item.replace("\r", "")
                 $.ajax({
                    url: '/polls/search/',
                    type: 'post', // This is the default though, you don't actually need to always mention it
                    dataType: "json",
                    beforeSend: function() {
                        // setting a timeout
                        $('.for_loader').css("display", "block");
                        var counter = parseInt($('#waiting_response').val());
                        counter++;
                        $('#waiting_response').val(counter);
                        console.log('counter=' + counter);
                    },
                     data: query + '&search_query=&extra_urls=' + item,
                     success: function(data) {
                        console.log(data);
                        $('#table_of_links').append
                        ( '<tr>'+
                            '<td style="word-wrap: break-word;">'+ '<a href="' + data.url +'">'+data.url+'</a></td>'+
                            '<td>'+ data.words_count +'</td>'+
                            '<td>'+ data.classic +'</td>'+
                            '<td>'+ data.academ +'</td>'+
                            '<td>'+ data.vodnost +'</td>'+
                            '<td>'+ data.spam +'</td>'+
                            '<td>'+ data.ciphs +'</td>'+
                            '<td>'+ data.gunning +'</td>'+
                        '</tr>' );

                        csvRows.push(
                        [
                            data.url,
                            data.words_count,
                            data.classic,
                            data.academ,
                            data.vodnost,
                            data.spam,
                            data.ciphs,
                            data.gunning
                         ]
                         );

                     },
                     failure: function(data) {
                        console.log('fail');
                        //alert('Ошибка');
                     },
                     complete: function(data){
                        var counter = parseInt($('#waiting_response').val());
                        counter--;
                        console.log('counter=' + counter);
                        $('#waiting_response').val(counter);
                        if (counter<1){

                            $('.for_loader').css("display", "none");
                            var csvString = '';
                            csvRows.forEach(function(row) {
                                    csvString += row.join(',,,,');
                                    csvString += "\n";
                            });
                            a.href        = 'data:text/csv;charset=utf-8,' + encodeURI(csvString);
                            a.target      = '_blank';
                            a.download    = 'webinforceseo.csv';
                            //console.log(a);
                        }
                    }
                    });
            });

        }
        if ( config['search_query'].length > 0 ) {
            console.log('for search_query');
            console.log(query + '&search_query='+config['search_query']+'&extra_urls=');
            $('.for_loader2').css("display", "block");
            $.ajax({
                url: '/polls/search/',
                type: 'post', // This is the default though, you don't actually need to always mention it
                dataType: "json",
                beforeSend: function() {
                    // setting a timeout
                    $('.for_loader').css("display", "block");
                    var counter = parseInt($('#waiting_response').val());
                    counter++;
                    $('#waiting_response').val(counter);
                    console.log('counter=' + counter);
                 },
                data: query + '&search_query=' + config['search_query'] + '&extra_urls=',
                success: function(data) {
                    console.log(data);
                    data['google'].map(function(item){
                        $('#table_of_google').append
                        ( '<tr>'+
                            '<td style="word-wrap: break-word;">'+ '<a href="' + item.url +'">'+item.url+'</a></td>'+
                            '<td>'+ item.words_count +'</td>'+
                            '<td>'+ item.classic +'</td>'+
                            '<td>'+ item.academ +'</td>'+
                            '<td>'+ item.vodnost +'</td>'+
                            '<td>'+ item.spam +'</td>'+
                            '<td>'+ item.ciphs +'</td>'+
                            '<td>'+ item.gunning +'</td>'+
                        '</tr>' );
                        csvRows.push([
                            item.url,
                            item.words_count,
                            item.classic,
                            item.academ,
                            item.vodnost,
                            item.spam,
                            item.ciphs,
                            item.gunning
                            ]);
                 });
                    data['yandex'].map(function(item){
                        $('#table_of_yandex').append
                        ( '<tr>'+
                            '<td style="word-wrap: break-word;">'+ '<a href="' + item.url +'">'+item.url+'</a></td>'+
                            '<td>'+ item.words_count +'</td>'+
                            '<td>'+ item.classic +'</td>'+
                            '<td>'+ item.academ +'</td>'+
                            '<td>'+ item.vodnost +'</td>'+
                            '<td>'+ item.spam +'</td>'+
                            '<td>'+ item.ciphs +'</td>'+
                            '<td>'+ item.gunning +'</td>'+
                        '</tr>' );
                        csvRows.push([
                            item.url,
                            item.words_count,
                            item.classic,
                            item.academ,
                            item.vodnost,
                            item.spam,
                            item.ciphs,
                            item.gunning
                            ]);
                 });
                },
                failure: function(data) {
                    console.log('fail');
                    //alert('Ошибка');
                },
                complete: function(data){
                    var counter = parseInt($('#waiting_response').val());
                    counter--;
                    console.log('counter=' + counter);
                    $('#waiting_response').val(counter);
                    if (counter<1){
                         $('.for_loader').css("display", "none");
                        var csvString = '';
                        csvRows.forEach(function(row) {
                                csvString += row.join(',,,,');
                                csvString += "\n";
                        });
                        a.href        = 'data:text/csv;charset=utf-8,' + encodeURI(csvString);
                        a.target      = '_blank';
                        a.download    = 'webinforceseo.csv';
                        //console.log(a);
                    }
                }
            });

        }
        /*console.log( $('#myform').serialize() );*/
     });
     $('#download_csv').on('click', function(){
        console.log(a);
        a.click();
     });



});