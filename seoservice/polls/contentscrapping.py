from selenium import webdriver
from urllib.parse import quote


class ContentScrapping:
    """
    Данный класс позволяет парсить выдачу яндекса и гугла
    """
    user = None
    api_key = None
    yandex_url = None
    google_url = None
    query = None
    driver = None
    urls = []
    yandex_urls = []
    google_urls = []
    #парсим страниц из поисковой выдачи
    pages = []
    pages_from_google = []
    pages_from_yandex = []


    def __init__(self, query='', pages=1):

        self.user = None
        self.api_key = None
        self.yandex_url = None
        self.google_url = None
        self.query = None
        self.driver = None
        self.urls = []
        self.yandex_urls = []
        self.google_urls = []
        # парсим страниц из поисковой выдачи
        self.pages = []
        self.pages_from_google = []
        self.pages_from_yandex = []

        self.query = quote(query)
        self.pages = pages
        if self.query != '':
            self.driver = webdriver.PhantomJS()
            self.driver.set_page_load_timeout(60)
            self.__get_yandex_urls__()
            self.__get_google_urls__()
            self.driver.close()
            self.driver = None

    def __get_yandex_urls__(self):
        # yandex
        get_query = 'https://yandex.ru/search/xml?user=konstantinLitkevich&key=03.123611872:a36d1da3f3af6723543d7940d077aa99&query='+self.query
        for i in range(self.pages):
            try:
                self.driver.get(get_query + '&page=' + str(i))
            except:
                print('yandex err')
                break
            else:
                elements = (self.driver.find_elements_by_tag_name('url'))
                for element in elements:
                    self.yandex_urls.append(element.text)
                    print('yandex element' + element.text)

    def __get_google_urls__(self):

        get_query = 'https://www.google.ru/search?q='+self.query

        for i in range(self.pages):
            try:
                self.driver.get(get_query + '&start='+str((i+1)*10 - 10))
            except:
                print('except')
                break
            else:
                print('google page '+str(i))
                #elements = (self.driver.find_elements_by_tag_name('cite'))
                links = self.driver.find_elements_by_css_selector('div>h3.r>a')
                print(links)
                for a in links:
                    #self.urls.append(element.text)
                    self.driver.execute_script("window.open('');")
                    page = a.get_attribute('href')
                    self.driver.switch_to.window(self.driver.window_handles[1])
                    try:
                        self.driver.get(page)
                    except:
                        print('except')
                        self.driver.close()
                        self.driver.switch_to.window(self.driver.window_handles[0])
                    else:
                        print('else')
                        self.google_urls.append(self.driver.current_url)
                        self.driver.close()
                        self.driver.switch_to.window(self.driver.window_handles[0])
