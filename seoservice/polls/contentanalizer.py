from __future__ import unicode_literals
from selenium import webdriver
import justext
import re
import pymorphy2
from sklearn.feature_extraction.text import CountVectorizer
from math import sqrt
import string
import requests
import os
import sys

exclude = list(string.punctuation)


class ContentAnalizer:
    def __init__(self, url, is_vodnost = True, is_ciphs_law = True, is_toshnota = True, is_full_of_spam = True):

        self.vodnost = None
        self.ciphs_law = None
        self.classic_toshnota = None
        self.academ_toshnota = None
        self.frequency = None
        self.full_of_spam = None
        self.spam = None
        self.bigrams = None
        self.source = ''
        self.text = ''
        self.freqs = None
        self.normal_text = ''
        self.normal_text_list = []
        self.paragraphs = []
        self.word_counts = None
        self.url = None
        self.swl = [
        ]
        self.gunning = None

        print('init')
        module_dir = os.path.dirname(__file__)  # get current directory
        file_path = os.path.join(module_dir, 'res')
        file_path = os.path.join(file_path, 'stop_words_list.txt')
        file = open(file_path, 'r', encoding='utf-8')
        self.swl = file.readlines()
        self.swl = [line.replace('\n', '') for line in self.swl]
        file.close()

        file_path = os.path.join(module_dir, 'res')
        file_path = os.path.join(file_path, 'easy_words.txt')
        file = open(file_path, 'r', encoding='utf-8')
        self.easy_word_set = file.readlines()
        self.easy_word_set = [line.replace('\n', '') for line in self.easy_word_set]
        file.close()

        print('url_replace')
        self.url = url.replace('\n', '')
        self.url = self.url.replace('\r', '')
        #self.get_source(url)
        if not self.get_content(url):
            return
        #print(self.normal_text_list)
        self.word_counts = self.calculate_frequency(self.normal_text)
        if (self.word_counts == None):
            return
        self.gunning_fog(self.normal_text)
        self.calculate_academ_toshnota(self.normal_text_list, self.word_counts)
        frqs = [i[1] for i in self.word_counts]
        self.calculate_full_of_spam(frqs)
        self.calculate_ciphs_law(frqs)
        self.calculate_classic_toshnota(frqs)
        self.calculate_vodnost(self.normal_text_list)

    def get_source(self, url):
        response = requests.get(url)
        if response.status_code == 200:
            page = response.content
            self.paragraphs = justext.justext(page,
                                              justext.get_stoplist("Russian"),
                                              length_low=70,
                                              length_high=200, stopwords_low=0.3,
                                              stopwords_high=0.32,
                                              max_link_density=0.2,
                                              max_heading_distance=200,
                                              no_headings=False,
                                              )
            #self.paragraphs = [p.__dict__ for p in self.paragraphs]
            return self.paragraphs
        else:
            return response.status_code

    def get_content(self, url):
        try:
            response = requests.get(url)
        except:
            return
        if response.status_code == 200:
            page = response.content
            morph = pymorphy2.MorphAnalyzer()
            paragraphs = justext.justext(page,
                                         justext.get_stoplist("Russian"),
                                         length_low=70,
                                         length_high=200,
                                         stopwords_low=0.3,
                                         stopwords_high=0.32,
                                         max_link_density=0.2,
                                         max_heading_distance=200,
                                         no_headings=False
                                         )
            #content?????
            p_list = [p.text for p in paragraphs]
            self.text = ' '.join([p.text for p in paragraphs if p.class_type == 'good'])
            for word in self.text.split():
                value = morph.parse(word)[0]
                self.normal_text_list.append(value.normal_form)
            self.normal_text = ' '.join(self.normal_text_list)
        else:
            return#response.status_code
        return self.text

    def calculate_params(self, normal_text):
        pass

    def calculate_vodnost(self, normal_text_list):
        stop_words_counter = 0
        for word in self.normal_text_list:
            if word in self.swl:
                stop_words_counter += 1

            #print(stop_words_counter)
        try:
            self.vodnost = 100*(stop_words_counter*1.00) / (len(self.normal_text_list)*1.00)
            self.vodnost = round(self.vodnost, 2)
#            return round(self.vodnost, 2)

        except:
            print("Error(GF): Word Count is Zero, cannot divide")
            return

    def calculate_ciphs_law(self, word_counts):
        """
        Закон Ципфа
        """
        print("Ципф")
        allowed_err_percent = 0.5
        word_frqs = sorted(word_counts, reverse=True)
        uniq = []
        [uniq.append(x) for x in word_frqs if x not in uniq]
        i = 1
        print(uniq)
        correspond = 0.0
        for frq in uniq[1:]:
            i += 1
            p = uniq[0]/i
            err = abs(p-frq)
            print('Ошибка ципфа = ' +str(err))
            print('допустимая ошибка Ципфа = ' + str(allowed_err_percent * p))
            if (allowed_err_percent * p) >= err:
                correspond += 1

        try:
            self.ciphs_law = (correspond/(len(uniq)-1.0))*100
            self.ciphs_law = round(self.ciphs_law, 2)
            return self.ciphs_law

        except:
            print("Error(GF): Word Count is Zero, cannot divide")
            return

    def calculate_classic_toshnota(self, word_counts):
        """
        Классическая тошнота
        """
        max_count = max(word_counts)
        self.classic_toshnota = round(sqrt(max_count), 2)

    def sentence_count(self, text):
        """
        Sentence count of a text
        """
        ignoreCount = 0
        sentences = re.split(r' *[\.\?!][\'"\)\]]* *', text)
        for sentence in sentences:
            if self.lexicon_count(sentence) <= 2:
                ignoreCount = ignoreCount + 1
        return max(1, len(sentences) - ignoreCount)

    def avg_sentence_length(self, text):
        lc = self.lexicon_count(text)
        sc = self.sentence_count(text)
        try:
            ASL = float(lc/sc)
            return round(lc/sc, 1)
        except:
            print("Error(ASL): Sentence Count is Zero, Cannot Divide")
            return

    def syllable_count(self, text):
        """
        Function to calculate syllable words in a text.
        I/P - a text
        O/P - number of syllable words
        """
        count = 0
        vowels = 'aeiouyеуыаоэяиюё'
        text = text.lower()
        text = "".join(x for x in text if x not in exclude)

        if text is None:
            return 0
        elif len(text) == 0:
            return 0
        else:
            if text[0] in vowels:
                count += 1
            for index in range(1, len(text)):
                if text[index] in vowels and text[index-1] not in vowels:
                    count += 1
            if text.endswith('e'):
                count -= 1
            if text.endswith('le'):
                count += 1
            if count == 0:
                count += 1
            count = count - (0.1*count)
            return count

    def lexicon_count(self, text, removepunct=True):
        """
        Function to return total lexicon (words in lay terms) counts in a text
        """
        if removepunct:
            text = ''.join(ch for ch in text if ch not in exclude)
        count = len(text.split())
        return count

    def avg_syllables_per_word(self, text):
        syllable = self.syllable_count(text)
        words = self.lexicon_count(text)
        try:
            ASPW = float(syllable)/float(words)
            return round(ASPW, 1)
        except:
            print("Error(ASyPW): Number of words are zero, cannot divide")
            return

    def difficult_words(self, text):
        """
        Расчёт сложности
        """
        text_list = text.split()
        diff_words_set = set()
        for value in text_list:
            if value not in self.easy_word_set:
                if self.syllable_count(value) > 1:
                    if value not in diff_words_set:
                        diff_words_set.add(value)
        return len(diff_words_set)

    def gunning_fog(self, normal_text_list):
        """
        Туманность Ганнинга
        """
        normal_text = ' '.join(normal_text_list[:150])
        try:
            per_diff_words = 100.0*float((self.difficult_words(normal_text)) / float(self.lexicon_count(normal_text)))+20
            self.gunning = 0.4*((self.avg_sentence_length(normal_text)) + per_diff_words)
            self.gunning = round(self.gunning, 2)
            return self.gunning
        except:
            print("Error(GF): Word Count is Zero, cannot divide")


    def calculate_academ_toshnota(self, normal_text_list, word_counts):
        """
        Расчёт академической тошноты
        """
        all_words = float(len(normal_text_list))
        if (all_words > 0 and word_counts):
            word_frqs = [i[1] for i in word_counts]
            word_frqs = sorted(word_frqs, reverse=True)
            print(word_frqs[0:5])
            self.academ_toshnota = (float(sum(word_frqs[0:5]))/all_words)*100
            self.academ_toshnota = round(self.academ_toshnota, 2)
        else:
            self.academ_toshnota = 0

    def calculate_frequency(self, normal_text, min_gram=1, max_gram=1, min_counter=1, swl=None):

        vectorizer = CountVectorizer(
            ngram_range=(min_gram, max_gram),
            stop_words=swl,
        )

        analyzer = vectorizer.build_analyzer()
        grams = analyzer(normal_text)
        try:
            X = vectorizer.fit_transform([normal_text])
        except:
            return
        else:
            terms = vectorizer.get_feature_names()
            # print(terms)
            freqs = X.sum(axis=0).A1
            # print(X.toarray())
            # result = dict(zip(terms, freqs))
            freqs = filter(lambda x: x > 0, freqs)
            freqs = [x.astype(float) for x in freqs]
            result = list(zip(terms, freqs))
            return result

    def calculate_bigrams(self, normal_text):
        bigrams = self.calculate_frequency(normal_text, min_gram=2, max_gram=2, min_counter=1, swl=self.swl)
        return bigrams

    def calculate_trigrams(self, normal_text):
        trigrams = self.calculate_frequency(normal_text, min_gram=3, max_gram=3, min_counter=1, swl=self.swl)
        return trigrams

    def calculate_full_of_spam(self, freqs):
        try:
            self.spam = 100 * float(sum(filter(lambda x: x > 2, freqs))) / sum(freqs)
            self.spam = round(self.spam, 2)
            return self.spam
        except:
            print("Error(GF): Word Count is Zero, cannot divide")
            return

class GroupAnalizer:
    all_keywords = None
    def __init__(self, **kwargs):
        keywords = None
        self.url_0 = None
        self.url_1 = None
        self.url_2 = None
        self.url_3 = None
        self.url_4 = None
        if kwargs['text'] and kwargs['url_0'] and kwargs['url_1'] and kwargs['url_2'] and kwargs['url_3'] and kwargs['url_4']:
            self.url_0 = ContentAnalizer(kwargs['url_0'])
            self.url_1 = ContentAnalizer(kwargs['url_1'])
            self.url_2 = ContentAnalizer(kwargs['url_2'])
            self.url_3 = ContentAnalizer(kwargs['url_3'])
            self.url_4 = ContentAnalizer(kwargs['url_4'])

    def get_keywords(self, all_urls):
        all_key_words = dict()
        for content in all_urls:
            for key in content.keys():
                if key in all_key_words:
                    all_key_words[key] += 1
                else:
                    all_key_words[key] = 1
