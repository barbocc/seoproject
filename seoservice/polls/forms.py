from django import forms
from django.utils.safestring import mark_safe

class SearchForm(forms.Form):
    search_query = forms.CharField(required=False,label='', max_length=500, widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'введите поисковой запрос'}))
    extra_urls = forms.CharField(required=False, label='', widget=forms.Textarea(
        attrs={'class': 'form-control', 'placeholder': 'Список url-ов для анализа', 'rows': "5", 'id': "analise-urls"}))
    stop_domains = forms.CharField(required=False, label='', max_length=500, widget=forms.Textarea(
        attrs={'class': 'form-control', 'placeholder': 'Список стоп сайтов', 'rows': "5", 'id': "stop-domains"}))

    #   качество текста по закону ципфа(https: // miratext.ru / seo_analiz_text)
    ciphs_law = forms.BooleanField(
        label='качество текста по закону ципфа',
        widget=forms.CheckboxInput(attrs={'class': 'form-check-input', 'checked': 'checked'}))
    #    + тошнота(адвего)
    classic_toshnota = forms.BooleanField(
        label='Классическая тошнота',
        widget=forms.CheckboxInput(attrs={'class': 'form-check-input', 'checked': 'checked'}))

    academ_toshnota = forms.BooleanField(
        label='Академическая  тошнота',
        widget=forms.CheckboxInput(attrs={'class': 'form-check-input', 'checked': 'checked'}))
    #    + водность(адвего)
    vodnost = forms.BooleanField(
        label='тошнота',
        widget=forms.CheckboxInput(attrs={'class': 'form-check-input', 'checked': 'checked'}))

    #    + частота слова в тексте(адвего)
    gunning = forms.BooleanField(
        label='Туманность Ганинга',
        widget=forms.CheckboxInput(attrs={'class': 'form-check-input', 'checked': 'checked'}))

    #    + заспамленность(text.ru)
    full_of_spam = forms.BooleanField(
        label='частотность',
        widget=forms.CheckboxInput(attrs={'class': 'form-check-input', 'checked': 'checked'}))

#    - оценка по главреду

    def send_email(self):
        #print(self.search_query)
        pass