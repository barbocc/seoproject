class DbRouter(object):
    """
    A router to control all database operations on models in the
    auth application.
    """
    def db_for_read(self, model, **hints):
        """
        Attempts to read remote models go to remote database.
        """
        if model._meta.label_lower == 'polls.wparticle':
            return 'wpsite'
        return 'default'

    def db_for_write(self, model, **hints):
        """
        Attempts to write remote models go to the remote database.
        """
        if model._meta.label_lower == 'polls.wparticle':
            return 'wpsite'
        return 'default'

    def allow_relation(self, obj1, obj2, **hints):
        """
        Do not allow relations involving the remote database
        """
        if obj1._meta.label_lower == 'polls.wparticle' or \
           obj2._meta.label_lower == 'polls.wparticle':
           return False
        return 'default'

    def allow_migrate(self, db, label_lower, model_name=None, **hints):
        """
        Do not allow migrations on the remote database
        """
        if label_lower == 'polls.wparticle':
            return False
        return True
